import { Component, ViewChild,ElementRef } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams } from 'ionic-angular';
import { GoogleMap, GoogleMaps, Environment, Marker } from '@ionic-native/google-maps';



declare var google: any;
/**
 * Generated class for the PlacePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-place',
  templateUrl: 'place.html',
})


export class PlacePage {
  @ViewChild('map') mapRef: ElementRef;
  map: any;
  lat:number;
  lng: number;


  constructor(public viewController: ViewController, public navCtrl: NavController, private navParams: NavParams) {
    this.lat = this.navParams.data.location.lat;
    this.lng = this.navParams.data.location.lng;

  }

  ionViewDidLoad(){
    this.showMap();
    console.log(this.mapRef);
  }

  showMap(){
    const location = new google.maps.LatLng(this.lat, this.lng);

    // map options
    const options = {
      center: location,
      zoom: 15,
      streetViewControl: false
    }

    const map = new google.maps.Map(this.mapRef.nativeElement,options);

    this.addMarker(location, map);
  }

  addMarker(position, map){
    return new google.maps.Marker({
      position, map
    })
  }

  onDismiss(){
    console.log("test of onDismiss");
    this.navCtrl.pop();
  }


}

import { Component } from '@angular/core';
import { NavController, ModalController } from 'ionic-angular';
import { NewPlacePage } from '../new-place/new-place';
import { PlacesService } from '../../services/places.services';
import { PlacePage } from '../place/place';
import { Place } from '../../Model/place.model';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  // properties places
  places: {title: string}[] = [];

  constructor(public navCtrl: NavController,
     private placesService: PlacesService,
     private modalCtr: ModalController) {

  }

  // method so that the places are getting update
  // if we do not do this then the new places will not be visible.
  ionViewWillEnter(){
    this.placesService.getPlaces()
    .then(
      (places) => this.places = places
    );
  }
  onLoadNewPlace(){

      this.navCtrl.push(NewPlacePage);
  }

  onOpenPlace(place: Place){
    this.modalCtr.create(PlacePage, place).present();
  }
}

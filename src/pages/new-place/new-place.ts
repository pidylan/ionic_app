import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { PlacesService } from '../../services/places.services';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-new-place',
  templateUrl: 'new-place.html',
})




export class NewPlacePage {
  location: {lat:number, lng: number} = {lat: 0, lng:0};


  constructor(private placesService: PlacesService, private navCtrl: NavController, private geolocation: Geolocation) {

  }


// form value met als propertie title van het type string.
  onAddPlace(value: {title: string}){
    this.placesService.addPlace({title: value.title, location: this.location});
    // pop de bovenste pagina van de pagina stapel.
    this.navCtrl.pop();


  }



  onLocateUser(){
    this.geolocation.getCurrentPosition()
    .then(
      (location)=>{
        console.log('location fetched successfully');
        this.location.lat = location.coords.latitude;
        this.location.lng = location.coords.longitude;

        var AddPlaceButton = <HTMLInputElement> document.getElementById("AddPlaceButton");
        AddPlaceButton.disabled = false;
      }




    )
    .catch(
      (error) => console.log('an error occured' + error)
    );
  }


}
